﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;
using System.Globalization;

namespace DipMag_v1
{
    class LogWriter
    {
        public LogWriter(){}

        public void add(string logLine)
        {
            string logFile = @"log/" + DateTime.Today.ToString("d") + ".txt";

            if (!File.Exists(logFile)) {
                File.Create(logFile).Close();
            }
            string[] log = { "[" + DateTime.Now.ToString("G") + "]\t" + logLine };
            File.AppendAllLines(logFile, log);
        }
    }
}
