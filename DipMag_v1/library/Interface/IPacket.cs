﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipMag_v1.library
{
    interface IPacket
    {
        byte[] getPacket();
    }
}
