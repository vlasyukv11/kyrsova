﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DipMag_v1.library
{
    public class ClientsInfo
    {
        public NetworkInterface adapter;
        protected WorkPlace _form;
        public ClientsInfo(NetworkInterface network, WorkPlace form)
        {
            adapter = network;
            _form = form;
        }

        /// <summary>
        /// визначення активних ір адресів в мережі (ping): включає методи - fetchAllIp та ping
        /// </summary>
        /// <returns>List<IPAddress> actIP</returns>
        public List<IPAddress> fetchActiveIp()
        {
            List<IPAddress> actIP = new List<IPAddress>();

            return ping(fetchAllIp(Network_math.getAllAddresses(Interface_info.inteface_information(adapter)[1])));
        }

        /// <summary>
        /// Вибірка усіх ІР адрес в мережі які доступні в мережі
        /// </summary>
        /// <param name="maxNumberIp">Int32</param>
        /// <returns> IPAddress[] listIPAdd</returns>
        public IPAddress[] fetchAllIp(int maxNumberIp)
        {
            IPAddress dhcpNetwAdd = Interface_info.netw_addr(Interface_info.fetch_dhcp_addr(adapter)
                , Interface_info.inteface_information(adapter)[1]);

            IPAddress[] listIPAdd = new IPAddress[maxNumberIp];
            for (int j = 0; j < maxNumberIp - 1; j++) {
                IPAddress ipDiscover = Network_math.getNextIPAddress(dhcpNetwAdd);
                listIPAdd[j] = ipDiscover;
                dhcpNetwAdd = ipDiscover;
            }

            return listIPAdd;
        }

        /// <summary>
        /// Пінг мережі, формквання списку активних адрес
        /// </summary>
        /// <param name="listIPAdd">IPAddress[]</param>
        /// <returns>List<IPAddress> actIP</returns>
        public List<IPAddress> ping(IPAddress[] listIPAdd)
        {
            List<IPAddress> actIP = new List<IPAddress>();
            Ping ping = new Ping();
            int step = 1;
            Application.Current.Dispatcher.BeginInvoke(
                    new Action(() => _form.progress.Maximum = (listIPAdd.Length - 1)));
            // пошук активних ір
            var dhcpAddr = Interface_info.fetch_dhcp_addr(adapter);
            var myIp = Interface_info.inteface_information(adapter)[0];
            LogWriter logs = new LogWriter();
            for (int i = 0; i < listIPAdd.Length - 1; i++) {
                Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.progress.Value += step));
                IPAddress ip2Discover = listIPAdd[i];
                try {
                    if (ping.Send(ip2Discover, 30).Status == IPStatus.Success &&
                        ip2Discover != dhcpAddr
                            && ip2Discover != myIp) {
                        logs.add("Відправка Ping: packet[icmp], ip[" + ip2Discover.ToString() + "], status[Success]");
                        actIP.Add(ip2Discover);
                    }
                    else {
                        logs.add("Відправка Ping: packet[icmp], ip[" + ip2Discover.ToString() + "], status[Failure]");
                    }
                }
                catch (Exception e) {
                    throw new InvalidOperationException(e.ToString());
                }
            }

            return actIP;
        }

        // визначення mac address
        public List<string[]> fetchMacAddress(List<IPAddress> activeIP)
        {
            LibPcapLiveDevice selDevice = LibPcapLiveDeviceList.Instance[0];
            List<string[]> macList = new List<string[]>();

            foreach (LibPcapLiveDevice device in LibPcapLiveDeviceList.Instance) {
                if (device.Name == "\\Device\\NPF_" + adapter.Id) {
                    selDevice = device;
                    break;
                }
            }
            var ipInterface = Interface_info.inteface_information(adapter)[0];
            foreach (IPAddress ip in activeIP) {
                ARP arper = new ARP(selDevice);
                var macresolve = arper.Resolve(ip);
                string[] st = new string[3];
                st[1] = ip.ToString();
                st[2] = "MAC Resolve - false.";

                if (macresolve != null) {
                    st[2] = macresolve.ToString();
                }
                else if (ip == ipInterface) {
                    st[2] = adapter.GetPhysicalAddress().ToString();
                }
                macList.Add(st);
            }

            return macList;
        }
    }
}
