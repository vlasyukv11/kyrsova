﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace DipMag_v1.library
{
    /**
     * Клас який дає інформацію про мережу та інтерфейс пк
     * */
    class Interface_info
    {
        public static IPAddress fetch_dhcp_addr(NetworkInterface adapter)
        {
            IPAddressCollection addresses = adapter.GetIPProperties().DhcpServerAddresses;
            IPAddress dhcpServAdd = IPAddress.None;
            try {
                if (addresses.Count > 0) {
                    foreach (IPAddress address in addresses) {
                        dhcpServAdd = address; // адрес DHCP
                    }
                }
            }
            catch { return dhcpServAdd; }

            return dhcpServAdd;
        }

        public static IPAddress fetch_dns(IPAddressCollection dnsAddresses)
        {
            IPAddress dnsServAdd = IPAddress.None;
            try {
                if (dnsAddresses.Count > 0) {
                    foreach (IPAddress dns in dnsAddresses) {
                        dnsServAdd = dns; // адрес DNS
                    }
                }
            }
            catch { return dnsServAdd; }

            return dnsServAdd;
        }

        public static IPAddress fetch_gateway(NetworkInterface adapter)
        {
            IPAddress gatewayAdd = IPAddress.None;
            try {
                // адреса шлюзу по замовчуванню
                gatewayAdd = adapter.GetIPProperties().GatewayAddresses.FirstOrDefault().Address;
            }
            catch { return gatewayAdd; }

            return gatewayAdd;
        }

        public static IPAddress[] inteface_information(NetworkInterface adapter)
        {
            IPAddress dhcpMyAdd = IPAddress.None;
            IPAddress dhcpMyAddMask = IPAddress.None;
            IPAddress[] result = { dhcpMyAdd, dhcpMyAddMask };
            try {
                foreach (UnicastIPAddressInformation unicastIPAddressInformation in
                        adapter.GetIPProperties().UnicastAddresses) {
                    if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork && adapter.GetIPProperties().DhcpServerAddresses.Count>0) {
                        result[0] = unicastIPAddressInformation.Address; // Aадреса інтерфейса
                        result[1] = unicastIPAddressInformation.IPv4Mask; // маска інтерфейса
                        break;
                    }
                }
            }
            catch { return result; }

            return result;
        }

        public static IPAddress netw_addr(IPAddress dhcpServAdd, IPAddress dhcpMyAddMask)
        {
            IPAddress dhcpNetwAdd = IPAddress.None;

            try {
                dhcpNetwAdd = Network_math.getNetworkAddress(dhcpServAdd, dhcpMyAddMask); // адреса мережі
            }
            catch { return dhcpNetwAdd; }

            return dhcpNetwAdd;
        }

        public static PhysicalAddress fetchMacDHCPAddress(NetworkInterface adapter)
        {
            LibPcapLiveDevice selDevice = LibPcapLiveDeviceList.Instance[0];

            foreach (LibPcapLiveDevice device in LibPcapLiveDeviceList.Instance) {
                if (device.Name == "\\Device\\NPF_" + adapter.Id) {
                    selDevice = device;
                    break;
                }
            }

            ARP arp = new ARP(selDevice);
            var addr = fetch_dhcp_addr(adapter);
            var macresolve = arp.Resolve(addr);

            return macresolve;
        }

        WorkPlace _form;
        public void getAllInformation(NetworkInterface adapter, WorkPlace workPlace)
        {
            _form = workPlace;

            Info [] info = new Info[25];
            addToLV_3("Опис інтерфейсу", adapter.Description);
            addToLV_3("ID інтерфейсу", adapter.Id);
            string isReceive = adapter.IsReceiveOnly ? "Так" : "Ні";
            addToLV_3("Тільки прийом даних", isReceive);
            addToLV_3("Ім1я інтерфейсу", adapter.Name);
            addToLV_3("Тип мережевого інтерфейсу", adapter.NetworkInterfaceType.ToString());
            addToLV_3("Статус інтерфейсу", adapter.OperationalStatus.ToString());
            addToLV_3("Швидкість роботи інтерфейсу, біт/с", adapter.Speed.ToString());
            string isMulticast = adapter.SupportsMulticast ? "Так" : "Ні";
            addToLV_3("Підтримка широкоформатності", isMulticast);
            addToLV_3("Фізична адреса: ", adapter.GetPhysicalAddress().ToString());

            string versions = "";
            if (adapter.Supports(NetworkInterfaceComponent.IPv4)) {
                versions = "IPv4";
            }
            if (adapter.Supports(NetworkInterfaceComponent.IPv6)) {
                if (versions.Length > 0) {
                    versions += ", ";
                }
                versions += "IPv6";
            }
            addToLV_3("Підтримка версій", versions);

            IPInterfaceProperties prop = adapter.GetIPProperties();
            addToLV_3("DNS суфікс", prop.DnsSuffix);
            if (adapter.Supports(NetworkInterfaceComponent.IPv4)) {
                IPv4InterfaceProperties ipv4 = prop.GetIPv4Properties();
                addToLV_3("MTU", ipv4.Mtu.ToString());
            }

            string isDNSEnabled = prop.IsDnsEnabled ? "Так" : "Ні";
            string isDNSDynamic = prop.IsDynamicDnsEnabled ? "Так" : "Ні";

            addToLV_3("DNS активовано", isDNSEnabled);
            addToLV_3("Динамічний DNS", isDNSDynamic);

            string anycastAddresses = "";

            foreach (IPAddressInformation address in prop.AnycastAddresses) {
                if (address != prop.AnycastAddresses.Last()) {
                    anycastAddresses += address.Address + ", ";
                }
                else {
                    anycastAddresses += address.Address;
                }
            }
            addToLV_3("Anycast адреси", anycastAddresses);

            string multicastAddresses = "";
            foreach (MulticastIPAddressInformation address in prop.MulticastAddresses) {
                if (address.Address.AddressFamily == AddressFamily.InterNetwork) {
                    if (address != prop.MulticastAddresses.Last()) {
                        multicastAddresses += address.Address + ", ";
                    }
                    else {
                        multicastAddresses += address.Address.ToString();
                    }
                }
            }
            addToLV_3("Multicast адреси", multicastAddresses);

            string unicastAddresses = "";
            foreach (UnicastIPAddressInformation address in prop.UnicastAddresses) {
                if (address.Address.AddressFamily == AddressFamily.InterNetwork) {
                    if (address != prop.UnicastAddresses.Last()) {
                        unicastAddresses += address.Address + ", ";
                    }
                    else {
                        unicastAddresses += address.Address.ToString();
                    }
                }
            }
            addToLV_3("Unicast адреси", unicastAddresses);
        }

        public void addToLV_3 (string param, string address)
        {
               System.Windows.Application.Current.Dispatcher.BeginInvoke(
                    new Action(() => _form.listView.Items.Add(new Info()
                    {
                        param = param,
                        address = address,
                    })));
        }
    }

   
}
