﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace DipMag_v1.library
{
    class DHCPDiscover
    {
        private readonly byte[] op = { 0x01 };
        private readonly byte[] htype = { 0x01 };
        private readonly byte[] hlen = { 0x06 };
        private readonly byte[] hops = { 0x00 };
        private readonly byte[] xid; // Transaction Id 
        private readonly byte[] secs = { 0x00, 0x00 };
        private readonly byte[] flags = { 0x00, 0x00 };
        private readonly byte[] ciaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] yiaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] siaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] giaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] chaddr; // hardware address
        private readonly byte[] clientHwAdd = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] magicCookie = { 0x63, 0x82, 0x53, 0x63 };
        private readonly byte[] messageType = { 0x35, 0x01, 0x01 }; // DHCP message Type Discover
        private readonly byte[] messageType1 = { 0x74, 0x01, 0x01 }; // DHCP auto-conf
        private readonly byte[] messageType2; // Client identifier
        private readonly byte[] messageType3; // Host name
        private readonly byte[] messageType4; //Vendor class identifier
        private readonly byte[] messageType5 = { 0x37, 0x0B, 0x01, 0x0F, 0x03, 0x06, 0x2C, 
                                                   0x2E, 0x2F, 0x1F, 0x21, 0xF9, 0x2B }; //Parametr request list
        private readonly byte[] messageType6; //Vendor-Specific information
        private readonly byte[] sname = new byte[64];
        private readonly byte[] file = new byte[128];
        private readonly byte[] end = { 0xff };

        public DHCPDiscover(PhysicalAddress hardaddr, byte []idb)
        {
            chaddr = hardaddr.GetAddressBytes();

            xid = idb;

            byte[] mT2 = { 0x3D, 0x07, 0x01, chaddr[0], chaddr[1], chaddr[2], chaddr[3], chaddr[4], chaddr[5] };
            messageType2 = mT2;

            
            byte[] mT3 = { 0x0C, 0x0F, 0x68, 0x6F, 0x6d, 0x65, 0x2D, 0x36, 0x65, 0x64,
                             0x65, 0x39, 0x30, 0x63, 0x36, 0x31, 0x65 };
            messageType3 = mT3;

            byte[] mT4 = { 0x3C, 0x08, 0x4D, 0x53, 0x46, 0x54, 0x20, 0x35, 0x2E, 0x30 };
            messageType4 = mT4;

            byte[] mT6 = { 0x2B, 0x02, 0xDC, 0x00 };
            messageType6 = mT6;
        }

        public byte[] getDiscoveryPacket()
        {
            byte[] packet = op.Concat(htype)
                            .Concat(hlen)
                            .Concat(hops)
                            .Concat(xid)
                            .Concat(secs)
                            .Concat(flags)
                            .Concat(ciaddr)
                            .Concat(yiaddr)
                            .Concat(siaddr)
                            .Concat(giaddr)
                            .Concat(chaddr)
                            .Concat(clientHwAdd)
                            .Concat(sname)
                            .Concat(file)
                            .Concat(magicCookie)
                            .Concat(messageType)
                            .Concat(messageType1)
                            .Concat(messageType2)
                            .Concat(messageType3)
                            .Concat(messageType4)
                            .Concat(messageType5)
                            .Concat(messageType6)
                            .Concat(end)
                            .ToArray();

            return packet;
        }
    }
}
