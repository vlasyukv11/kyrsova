﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.IO;
using System.Windows;

namespace DipMag_v1.library.DHCPPackets
{
    class DHCPOffer : IPacket
    {
        private readonly byte[] op = { 0x02 };
        private readonly byte[] htype = { 0x01 };
        private readonly byte[] hlen = { 0x06 };
        private readonly byte[] hops = { 0x00 };
        private readonly byte[] xid; // Transaction Id 
        private readonly byte[] secs = { 0x00, 0x00 };
        private readonly byte[] flags = { 0x00, 0x00 };
        private readonly byte[] ciaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] yiaddr;//мінять//
        private readonly byte[] siaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] giaddr = { 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] chaddr; // hardware address
        private readonly byte[] clientHwAdd = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
        private readonly byte[] sname = new byte[64];
        private readonly byte[] file = new byte[128];
        private readonly byte[] magicCookie = { 0x63, 0x82, 0x53, 0x63 };
        private readonly byte[] messageType = { 0x35, 0x01, 0x02 }; // DHCP message Type Offer
        private readonly byte[] messageType1; // DHCP server identifier 36 04 .....
        private readonly byte[] messageType2 = {0x33, 0x04, 0x00, 0x00, 0x1C, 0x20 }; // IP address lease time 33 04 ....
        private readonly byte[] messageType3; // Subnet mask 01 04 ....... 
        private readonly byte[] messageType4 ; // Router 03 04 ..........
        private readonly byte[] messageType5 ; // DNS 06 04 .........
        //private readonly byte[] messageType6 = { }; //Vendor-Specific information
        private readonly byte[] end = { 0xff };
        private readonly byte[] padding={};//274


        public DHCPOffer(string[] defaultSettings, byte[] id, IPAddress yiaddr1, PhysicalAddress chaddr1)
        {
            xid = id;
            yiaddr = yiaddr1.GetAddressBytes();
            chaddr = chaddr1.GetAddressBytes();
            byte[] b1 = (IPAddress.Parse(defaultSettings[0].Trim())).GetAddressBytes(); ;
            byte[] mType1 = {0x36, 0x04, b1[0],b1[1],b1[2],b1[3] };
            messageType1 = mType1;

            b1 = IPAddress.Parse(defaultSettings[2].Trim()).GetAddressBytes();
            byte[] b3 = { 0x01, 0x04, b1[0], b1[1], b1[2], b1[3] };
            messageType3 = b3;

            b1 = IPAddress.Parse(defaultSettings[0].Trim()).GetAddressBytes();
            byte[] b4 = { 0x03, 0x04, b1[0], b1[1], b1[2], b1[3] };
            messageType4=b4;

            b1 = IPAddress.Parse(defaultSettings[3].Trim()).GetAddressBytes();
            byte[] b5 = { 0x06, 0x04, b1[0], b1[1], b1[2], b1[3] };
            messageType5= b5;

        }

        public byte[] getPacket()
        {
            byte[] packet = op.Concat(htype)
                            .Concat(hlen)
                            .Concat(hops)
                            .Concat(xid)
                            .Concat(secs)
                            .Concat(flags)
                            .Concat(ciaddr)
                            .Concat(yiaddr)
                            .Concat(siaddr)
                            .Concat(giaddr)
                            .Concat(chaddr)
                            .Concat(clientHwAdd)
                            .Concat(sname)
                            .Concat(file)
                            .Concat(magicCookie)
                            .Concat(messageType)
                            .Concat(messageType1)
                            .Concat(messageType2)
                            .Concat(messageType3)
                            .Concat(messageType4)
                            .Concat(messageType5)
                            .Concat(end)
                            .Concat(padding)
                            .ToArray();

            return packet;
        }
    }
}
