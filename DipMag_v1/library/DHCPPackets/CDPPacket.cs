﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace DipMag_v1.library.DHCPPackets
{
    class CDPPacket : IPacket
    {
        private byte[] ieee = { 0x01, 0x00, 0x0c, 0xcc, 0xcc, 0xcc };
        private readonly byte[] llc = { 0xaa, 0xaa, 0x03, 0x00, 0x00, 0x0c, 0x20, 0x00 };
        private readonly byte[] version = { 0x02 };
        private readonly byte[] ttl = { 0xb4 };
        private readonly byte[] checksum = { 0x00, 0x00 };
        private readonly byte[] device_id_type = { 0x00, 0x01 };
        private readonly byte[] device_id_len = { };
        private readonly byte[] device_id = { };
        private readonly byte[] software_version_type = { 0x00, 0x05 };
        private readonly byte[] software_version_len = {};
        private readonly byte[] software_version = {};
        private readonly byte[] platform_type = { 0x00, 0x06 };
        private readonly byte[] platform_len = { };
        private readonly byte[] platform = { };
        private readonly byte[] addresses = { 0x00 , 0x02 , 0x00 , 0x11 , 0x00 , 0x00 , 0x00
, 0x01 , 0x01 , 0x01 , 0xcc , 0x00 , 0x04 , 0x00 , 0x00 , 0x00 , 0x00};
        private readonly byte[] port_id_type = { 0x00, 0x03 };
        private readonly byte[] port_id_len = { };
        private readonly byte[] port_id = { };
        private readonly byte[] capabilities = { 0x00 , 0x04
, 0x00 , 0x08 , 0x00 , 0x00 , 0x00 , 0x28};
        private byte[] hello = { 0x00 , 0x08 , 0x00 , 0x24 , 0x00 , 0x00 , 0x0c , 0x01 , 0x12 , 0x00
, 0x00 , 0x00 , 0x00 , 0xff , 0xff , 0xff , 0xff , 0x01 , 0x02 , 0x20 , 0xff , 0x00 , 0x00 , 0x00 , 0x00 , 0x00
, 0x00 };
        private readonly byte[] vtp = { 0x00, 0x09, 0x00, 0x04 };
        private readonly byte[] vlan = { 0x00 , 0x0a
, 0x00 , 0x06 , 0x00 , 0x01};
        private readonly byte[] duplex = { 0x00, 0x0b, 0x00, 0x05, 0x01 };
        private readonly byte[] bitmap = { 0x00, 0x12, 0x00, 0x05, 0x00 };
        private readonly byte[] untrusted = { 0x00 , 0x13
, 0x00 , 0x05 , 0x00};
        private readonly byte[] managment_addr = { 0x00 , 0x16 , 0x00 , 0x11 , 0x00 , 0x00 , 0x00 , 0x01 , 0x01 , 0x01 , 0xcc , 0x00 , 0x04
, 0x00 , 0x00 , 0x00 , 0x00};
        private readonly byte[] power_avaliable = { 0x00 , 0x1a , 0x00 , 0x10 , 0x00 , 0x00 , 0x00 , 0x01 , 0x00 , 0x00 , 0x00 , 0x00
, 0xff , 0xff , 0xff , 0xff};
        
        public CDPPacket(PhysicalAddress addr, WorkPlace form)
        {
            byte[] ieee_end = { 0x01, 0x76 };
            byte[] hello_end = { 0xff, 0x00, 0x00 };
            var ieee_buf = ieee;
            ieee_buf = ieee_buf.Concat(addr.GetAddressBytes()).Concat(ieee_end).ToArray();
            hello = hello.Concat(addr.GetAddressBytes()).Concat(hello_end).ToArray();
            CDPConfig settings = form.cdpConf;

            
            device_id = Encoding.ASCII.GetBytes(settings.deviceID);
            device_id_len = Convert_int_byte(device_id.Length+4);
            software_version = Encoding.ASCII.GetBytes(settings.sVersion);
            software_version_len = Convert_int_byte(software_version.Length+4);
            port_id = Encoding.ASCII.GetBytes(settings.portID);
            port_id_len = Convert_int_byte(port_id.Length+4);
            platform = Encoding.ASCII.GetBytes(settings.platform);
            platform_len = Convert_int_byte(platform.Length+4);

            byte[] packet = ieee_buf.Concat(llc)
                            .Concat(version)
                            .Concat(ttl)
                            .Concat(checksum)
                            .Concat(device_id_type)
                            .Concat(device_id_len)
                            .Concat(device_id)
                            .Concat(software_version_type)
                            .Concat(software_version_len)
                            .Concat(software_version)
                            .Concat(platform_type)
                            .Concat(platform_len)
                            .Concat(platform)
                            .Concat(addresses)
                            .Concat(port_id_type)
                            .Concat(port_id_len)
                            .Concat(port_id)
                            .Concat(capabilities)
                            .Concat(hello)
                            .Concat(vtp)
                            .Concat(vlan)
                            .Concat(duplex)
                            .Concat(bitmap)
                            .Concat(untrusted)
                            .Concat(managment_addr)
                            .Concat(power_avaliable)
                            .ToArray();

           
            ieee = ieee.Concat(addr.GetAddressBytes()).Concat(Convert_int_byte(packet.Length - 14)).ToArray();
        }
        protected byte[] Convert_int_byte(int num)
        {
            byte[] bytes = new byte[2];
            bytes[0] = (byte)(num >> 8);
            bytes[1] = (byte)num;

            return bytes;
        }
        public byte[] getPacket()
        {       
            byte[] packet = ieee.Concat(llc)
                            .Concat(version)
                            .Concat(ttl)
                            .Concat(checksum)
                            .Concat(device_id_type)
                            .Concat(device_id_len)
                            .Concat(device_id)
                            .Concat(software_version_type)
                            .Concat(software_version_len)
                            .Concat(software_version)
                            .Concat(platform_type)
                            .Concat(platform_len)
                            .Concat(platform)
                            .Concat(addresses)
                            .Concat(port_id_type)
                            .Concat(port_id_len)
                            .Concat(port_id)
                            .Concat(capabilities)
                            .Concat(hello)
                            .Concat(vtp)
                            .Concat(vlan)
                            .Concat(duplex)
                            .Concat(bitmap)
                            .Concat(untrusted)
                            .Concat(managment_addr)
                            .Concat(power_avaliable)
                            .ToArray();

            return packet;
        }

    }    
}
