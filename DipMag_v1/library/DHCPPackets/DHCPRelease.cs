﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;

namespace DipMag_v1.library.DHCPPackets
{
    class DHCPRelease : IPacket
    {
        private readonly byte[] BPBootpFlags = { 0x00, 0x00 }; //Bootp flags: 0x0000 (Unicast)

        private readonly byte[] BPClientHardwareAddressPadding =
        {
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00
        }; //Client hardware address padding: 00000000000000000000

        private readonly byte[] BPClientIPAddress; //Client IP address: 192.168.0.100 (0.0.0.0)
        private readonly byte[] BPClientMAC;
        private readonly byte[] BPClientMACAddress;

        private readonly byte[] BPDHCPMessageType = { 0x35, 0x01, 0x07 };
        //Option: (53) DHCP Message Type (Request) => Length: 1, DHCP: Release (1)

        private readonly byte[] BPHardwareAddressLength = { 0x06 }; //Hardware address length: 6
        private readonly byte[] BPHardwareType = { 0x01 }; //Hardware type: Ethernet (0x01)
        private readonly byte[] BPHops = { 0x00 }; //Hops: 0
        private readonly byte[] BPMagicCookie = { 0x63, 0x82, 0x53, 0x63 }; //Magic cookie: DHCP
        private readonly byte[] BPMessageType = { 0x01 }; //Message type: Boot Request (1)

        private readonly byte[] BPMyIPAddress = { 0x00, 0x00, 0x00, 0x00 }; //Your (client) IP address: 0.0.0.0 (0.0.0.0)

        private readonly byte[] BPNextServerIPAddress = { 0x00, 0x00, 0x00, 0x00 };
        //Next server IP address: 0.0.0.0 (0.0.0.0)

        private readonly byte[] BPRelayAgentIPAddress = { 0x00, 0x00, 0x00, 0x00 };
        //Relay agent IP address: 0.0.0.0 (0.0.0.0)

        private readonly byte[] BPSecondsElapsed = { 0x00, 0x00 }; //Seconds elapsed: 0

        private readonly byte[] BPServerHostName = new byte[64];

        private readonly byte[] BPServerIdentifier; //Option: (54) DHCP Server Identifier ()
        private readonly byte[] BootFileName = new byte[128];
        private readonly byte[] END = { 0xff }; //Option End: 255

        private readonly byte[] Padding = new byte[60];
        private byte[] BPTransaction;

        public DHCPRelease(PhysicalAddress clientHwAddress, IPAddress clientIpAddress, IPAddress dhcpServerIpAddress)
        {
            BPClientMACAddress = clientHwAddress.GetAddressBytes();
            setTransactionId();

            BPClientIPAddress = clientIpAddress.GetAddressBytes();

            var firstByte = new byte[] { 0x36, 0x04 };
            BPServerIdentifier = firstByte.Concat(dhcpServerIpAddress.GetAddressBytes()).ToArray();
          
            var _part = new byte[] { 0x3d, 0x07, 0x01 };

            BPClientMAC = _part.Concat(clientHwAddress.GetAddressBytes()).ToArray(); 
        }

        private void setTransactionId()
        {
            var r = new Random();
            var transactionBytes = new byte[4];
            r.NextBytes(transactionBytes);
            BPTransaction = transactionBytes;
        }

        private void setClientId(PhysicalAddress clHwAdd)
        {
            byte[] data = clHwAdd.GetAddressBytes();
        }

        public byte[] getPacket()
        {
            byte[] result =
                BPMessageType.Concat(BPHardwareType)
                    .Concat(BPHardwareAddressLength)
                    .Concat(BPHops)
                    .Concat(BPTransaction)
                    .Concat(BPSecondsElapsed)
                    .Concat(BPBootpFlags)
                    .Concat(BPClientIPAddress)
                    .Concat(BPMyIPAddress)
                    .Concat(BPNextServerIPAddress)
                    .Concat(BPRelayAgentIPAddress)
                    .Concat(BPClientMACAddress)
                    .Concat(BPClientHardwareAddressPadding)
                    .Concat(BPServerHostName)
                    .Concat(BootFileName)
                    .Concat(BPMagicCookie)
                    .Concat(BPDHCPMessageType).Concat(BPClientMAC).Concat(BPServerIdentifier)
                    .Concat(END)
                    .Concat(Padding)
                    .ToArray();

            return result;
        }
    }
}
