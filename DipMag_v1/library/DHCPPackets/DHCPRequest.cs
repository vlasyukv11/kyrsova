﻿using System;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;

namespace DipMag_v1.library.DHCPPackets
{
    class DHCPRequest : IPacket
    {
        private readonly byte[] request_op = { 0x01 };
        private readonly byte[] request_htype = { 0x01 };
        private readonly byte[] request_hlen = { 0x06 };
        private readonly byte[] request_hops = { 0x00 };
        private byte[] request_xid; // Transaction Id 
        private readonly byte[] request_secs = { 0x00, 0x00 };
        private readonly byte[] request_flags = { 0x00, 0x00 };
        private byte[] request_ciaddr; //Client ip (new ip offer)
        private readonly byte[] request_yiaddr = { 0x00, 0x00, 0x00, 0x00 }; // your client ip
        private readonly byte[] request_siaddr = { 0x00, 0x00, 0x00, 0x00 }; // next serv ip
        private readonly byte[] request_giaddr = { 0x00, 0x00, 0x00, 0x00 }; // relay agent ip
        private byte[] request_chaddr; // hardware address 
        private readonly byte[] request_clientHwAdd = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; //
        private readonly byte[] request_sname = new byte[64];
        private readonly byte[] request_file = new byte[128];
        private readonly byte[] request_magicCookie = { 0x63, 0x82, 0x53, 0x63 };
        private readonly byte[] request_messageType = { 0x35, 0x01, 0x03 }; // DHCP message Type Discover (53)
        private byte[] request_messageType1; // DHCP server identifier (54) (server ip)
        private byte[] request_messageType2; // Rewuested ip add (50) (new ip offer)
        private byte[] request_messageType3; // Client identifier (61) (my mac)
        private byte[] request_messageType4;//= { 0x0C, 0x04, 0x50, 0x43, 0x31, 0x31 }; // Host name (12)
        private readonly byte[] request_messageType5 = { 0x37, 0x04, 0x01, 0x03, 0x06, 0x0f }; //Parametr request list (55)
        private readonly byte[] request_end = { 0xff };

        public DHCPRequest(PhysicalAddress hardaddr, byte[] idb, IPAddress ciaddr, IPAddress servIP)
        {
            request_chaddr = hardaddr.GetAddressBytes();

            request_xid = idb;

            request_ciaddr = ciaddr.GetAddressBytes();

            byte[] mT1 = { 0x36, 0x04, servIP.GetAddressBytes()[0], servIP.GetAddressBytes()[1],
                                       servIP.GetAddressBytes()[2], servIP.GetAddressBytes()[3] };
            request_messageType1 = mT1;

            byte[] mT2 = { 0x32, 0x04, ciaddr.GetAddressBytes()[0], ciaddr.GetAddressBytes()[1],
                                       ciaddr.GetAddressBytes()[2], ciaddr.GetAddressBytes()[3] };
            request_messageType2 = mT2;

            byte[] mT3 = { 0x3D, 0x07, 0x01, request_chaddr[0], request_chaddr[1], request_chaddr[2], 
                                             request_chaddr[3], request_chaddr[4], request_chaddr[5] };
            request_messageType3 = mT3;

            Random rnd = new Random();
            byte[] mT4 = { 0x0C, 0x04, 
                             (byte)rnd.Next(1, 80), 
                             (byte)rnd.Next(1, 80), 
                             (byte)rnd.Next(1, 80), 
                             (byte)rnd.Next(1, 80) };
            request_messageType4 = mT4;

        }

        public byte[] getPacket()
        {
            byte[] packet = request_op.Concat(request_htype)
                            .Concat(request_hlen)
                            .Concat(request_hops)
                            .Concat(request_xid)
                            .Concat(request_secs)
                            .Concat(request_flags)
                            .Concat(request_ciaddr)
                            .Concat(request_yiaddr)
                            .Concat(request_siaddr)
                            .Concat(request_giaddr)
                            .Concat(request_chaddr)
                            .Concat(request_clientHwAdd)
                            .Concat(request_sname)
                            .Concat(request_file)
                            .Concat(request_magicCookie)
                            .Concat(request_messageType)
                            .Concat(request_messageType1)
                            .Concat(request_messageType2)
                            .Concat(request_messageType3)
                            .Concat(request_messageType4)
                            .Concat(request_messageType5)
                            .Concat(request_end)
                            .ToArray();

            return packet;
        }
    }
}
