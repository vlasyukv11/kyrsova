﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using PacketDotNet;
using System.Threading;
using System.ComponentModel;
using DipMag_v1.library;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace DipMag_v1.library
{
    class AtackProcedure
    {
        // Клас створений для збереження процедур атакування сервера
        WorkPlace _form;
        public AtackProcedure(WorkPlace formClass)
        {
            _form = formClass;
        }

        public bool DosAttack()
        {
            Dos dos = new Dos(_form);

            return dos.attack();
        }

        public bool SendRequest(Packet packet, UdpPacket udpPacket, IpPacket ipPacket, EthernetPacket ethPack)// second Starvation step
        {
            SendRequest sd = new SendRequest(_form, packet, udpPacket, ipPacket, ethPack);

            return sd.attack();
        }

        public bool MacFlooding()
        {
            MacFlooding dos = new MacFlooding(_form);

            return dos.attack();
        }

        public bool CDPFlooding()
        {
            CDP cdp = new CDP(_form);

            return cdp.attack();
        }
    }
}
