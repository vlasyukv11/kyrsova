﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace DipMag_v1.library
{
    class ClientsInfo
    {
        NetworkInterface adapter;
        Interface_info interface_data;
        Network_math net;
        public ClientsInfo(NetworkInterface network) { adapter = network; }

        /// <summary>
        /// визначення активних ір адресів в мережі (ping): включає методи - fetchAllIp та ping
        /// </summary>
        /// <returns>List<IPAddress> actIP</returns>
        public List<IPAddress> fetchActiveIp()
        {
            interface_data = new Interface_info();
            net = new Network_math();

            List<IPAddress> actIP = new List<IPAddress>();

            actIP = ping(fetchAllIp(net.getAllAddresses(interface_data.inteface_information(adapter)[1])));

            //findMac();
            return actIP;
        }

        /// <summary>
        /// Вибірка усіх ІР адрес в мережі які доступні в мережі
        /// </summary>
        /// <param name="maxNumberIp">Int32</param>
        /// <returns> IPAddress[] listIPAdd</returns>
        public IPAddress[] fetchAllIp(int maxNumberIp)
        {
            IPAddress dhcpNetwAdd = interface_data.netw_addr(interface_data.fetch_dhcp_addr(adapter)
                , interface_data.inteface_information(adapter)[1]);
            IPAddress[] listIPAdd = new IPAddress[maxNumberIp];

            for (int j = 0; j < maxNumberIp - 1; j++) {
                IPAddress ipDiscover = net.getNextIPAddress(dhcpNetwAdd);
                listIPAdd[j] = ipDiscover;
                dhcpNetwAdd = ipDiscover;
            }

            return listIPAdd;
        }

        /// <summary>
        /// Пінг мережі, формквання списку активних адрес
        /// </summary>
        /// <param name="listIPAdd">IPAddress[]</param>
        /// <returns>List<IPAddress> actIP</returns>
        public List<IPAddress> ping(IPAddress[] listIPAdd)
        {
            List<IPAddress> actIP = new List<IPAddress>();
            Ping ping = new Ping();
            double step = 100 / (listIPAdd.Length - 1);
            // пошук активних ір
            for (int i = 0; i < listIPAdd.Length - 1; i++) {
                IPAddress ip2Discover = listIPAdd[i];
                try {
                    if (ping.Send(ip2Discover, 30).Status == IPStatus.Success) {
                        if (ip2Discover != interface_data.fetch_dhcp_addr(adapter)
                            && ip2Discover != interface_data.inteface_information(adapter)[0]) {
                            actIP.Add(ip2Discover);
                        }
                    }
                }
                catch (Exception e) { System.Windows.MessageBox.Show(e.ToString(), "Невдалось відправити пінг"); }
            }

            return actIP;
        }

        // визначення mac address
        public List<string[]> fetchMacAddress(List<IPAddress> activeIP)
        {
            LibPcapLiveDevice selDevice = LibPcapLiveDeviceList.Instance[0];
            List<string[]> macList = new List<string[]>();
            Interface_info interfaceInfo = new Interface_info();

            var mDevice = LibPcapLiveDeviceList.Instance;

            foreach (LibPcapLiveDevice device in mDevice) {
                if (device.Name == "\\Device\\NPF_" + adapter.Id) {
                    selDevice = device;
                    break;
                }
            }

            foreach (IPAddress ip in activeIP) {
                ARP arper = new ARP(selDevice);
                var macresolve = arper.Resolve(ip);
                string[] st = new string[3];
                st[1] = ip.ToString();
                st[2] = "MAC Resolve - false.";

                if (macresolve != null) {
                    st[2] = macresolve.ToString();
                }
                else {
                    if (ip == interfaceInfo.inteface_information(adapter)[0]) {
                        st[2] = adapter.GetPhysicalAddress().ToString();
                    }
                }
                macList.Add(st);
            }

            return macList;
        }
    }
}
