﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using PacketDotNet;
using SharpPcap;
using SharpPcap.LibPcap;
using SharpPcap.WinPcap;

namespace DipMag_v1.library
{
    public class Network_math
    {
        // В цбьому класі зберігаються деякі корисні функції по створенню мережевих параметрів та їх перевірці
        Byte[] eth;
        UdpPacket udpPacket;
        const string wideIp = "255.255.255.255";

        public Network_math(Byte[] ethIn, UdpPacket udpPacketIn)
        {
            eth = ethIn;
            udpPacket = udpPacketIn;
        }

        public static PhysicalAddress GetRandomPhysicalAddress()
        {
            string hwAddress = "005079";
            var array = new[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
            var rnd = new Random();
            while (hwAddress.Length != 12) {
                hwAddress += array[rnd.Next(0, (array.Length - 1))];
            }

            return PhysicalAddress.Parse(hwAddress);
        }

        public static IPAddress getNextIPAddress(IPAddress ip)
        {
            string str = null;
            for (int i = 0; i < 4; i++) {
                string tmp = Convert.ToString(Convert.ToByte(ip.ToString().Split('.')[i]), 2);
                for (int _1 = 0; _1 < 8 - tmp.Length; _1++) {
                    str += "0";
                }
                str += tmp;
            }

            string newip = Convert.ToString(Convert.ToInt32(str, 2) + 1, 2);

            string str_buf = null;
            if (newip.Length != 32) {
                for (int _2 = 0; _2 < 32 - newip.Length; _2++) {
                    str_buf += "0";
                }
            }
            str_buf += newip;

            string ipadd = Convert.ToString(Convert.ToInt32(str_buf.Substring(0, 8), 2)) + "." +
                            Convert.ToString(Convert.ToInt32(str_buf.Substring(8, 8), 2)) + "." +
                            Convert.ToString(Convert.ToInt32(str_buf.Substring(16, 8), 2)) + "." +
                            Convert.ToString(Convert.ToInt32(str_buf.Substring(24, 8), 2));

            return IPAddress.Parse(ipadd);
        } // get next IP address

        public static IPAddress getNetworkAddress(IPAddress dhcpServAdd, IPAddress dhcpMyAddMask)
        {
            byte[] ipAdressBytes = dhcpServAdd.GetAddressBytes();
            byte[] subnetMaskBytes = dhcpMyAddMask.GetAddressBytes();
            var broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++) {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] & (subnetMaskBytes[i]));
            }

            return new IPAddress(broadcastAddress);
        }

        public static int getAllAddresses(IPAddress mask)// кількість ір адрес в мережі
        {
            int numberOfIps = 0;

            string str1 = null;
            string str2 = null;

            for (int i = 0; i < 4; i++) {
                string tmp1 = Convert.ToString(Convert.ToByte(wideIp.Split('.')[i]), 2);
                string tmp2 = Convert.ToString(Convert.ToByte(mask.ToString().Split('.')[i]), 2);

                for (int _1 = 0; _1 < 8 - tmp1.Length; _1++) {
                    str1 += "0";
                }
                for (int _2 = 0; _2 < 8 - tmp2.Length; _2++) {
                    str2 += "0";
                }

                str1 += tmp1;
                str2 += tmp2;
            }

            numberOfIps = Convert.ToInt32(str1, 2) - Convert.ToInt32(str2, 2);

            return numberOfIps;
        }

        public static string isOk(string ok)// parity bytes
        {
            if (ok.Length == 2) {
                return ok;
            }
            else {
                return "0" + ok;
            }
        }

        public string getBootstrap()
        {
            string bootstrap = "";
            for (int _data = 0; _data < udpPacket.PayloadData.Length; _data++) {
                bootstrap += udpPacket.PayloadData[_data].ToString() + "/";
            }

            return bootstrap;
        }

        public string getDestHw()
        {
            string destHw_str = "";
            for (int ii = 6; ii < 12; ii++) {
                destHw_str += isOk(Convert.ToString(eth[ii], 16).ToUpper());
            }

            return destHw_str;
        }

        public string getClientHw()
        {
            string clientHw_str = "";
            for (int ii = 0; ii < 6; ii++) {
                clientHw_str += isOk(Convert.ToString(eth[ii], 16).ToUpper());
            }
            return clientHw_str;
        }

        public byte[] getTransactionID()
        {
            byte[] transaction = {Convert.ToByte(udpPacket.PayloadData[4]), 
                                    Convert.ToByte(udpPacket.PayloadData[5]),
                                    Convert.ToByte(udpPacket.PayloadData[6]),
                                    Convert.ToByte(udpPacket.PayloadData[7])};


            return transaction;
        }
    }
}
