﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using DipMag_v1.library;
using System.IO;
using Microsoft.Win32;

namespace DipMag_v1
{
    /// <summary>
    /// Interaction logic for Atacks.xaml
    /// </summary>
    public partial class Atacks : Window
    {
        protected bool dos = false;
        protected bool starvation = false;
        protected bool release = false;
        protected bool fakeServer = false;
        WorkPlace work;
        protected bool is_inf = false;
        public Atacks(WorkPlace workPlace)
        {
            work = workPlace;

            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            work.Focus();
            ProgressBar progress = work.getProgress();
            ListView listView = work.getListView();
            ICaptureDevice captureInterface = work.getCaptureInterface();
            string packets = Int32.Parse(cout.Text) != 0 ? Int32.Parse(cout.Text).ToString() : "1";
            string timing = Int32.Parse(time.Text) != 0 ? Int32.Parse(time.Text).ToString() : "150";
            //work.toogle.Visibility = Visibility.Hidden;
            work.unlim = is_inf;
            if (dos == true) {
                work.MenuItem_Click(sender, e);
                work.setNumAttack(WorkPlace.dos);
                work.toogle.IsEnabled = false;
                work.start(timing, packets, progress, listView, captureInterface);
            }
            if (starvation == true) {
                work.MenuItem_Click_1(sender, e);
                work.setNumAttack(WorkPlace.starvation);
                work.toogle.IsEnabled = false;
                work.start(timing, packets, progress, listView, captureInterface);
            }
            if (release == true) {
                WorkPlace workPlace = new WorkPlace(work.adapter);
                workPlace.MenuItem_Click_2(sender, e);
                workPlace.setNumAttack(WorkPlace.release);
                workPlace.toogle.IsEnabled = false;
                workPlace.start(timing, packets, progress, listView, captureInterface);
                workPlace.Show();
            }
            if (fakeServer == true) {
                Scenario scen = new Scenario(captureInterface);
                scen.serverIP.Text=serverIP.Text;
                scen.serverMAC.Text=serverMAC.Text; 
                scen.serverMask.Text=serverMask.Text;
                scen.serverDNS.Text = serverDNS.Text; 
                scen.startIP.Text = startIP.Text;
                scen.toogle.IsEnabled=false;
                scen.open.IsEnabled = false;
                scen.save.IsEnabled = false;
                scen.Show();
                scen.start();
            }
        }

        private void Dos_Checked(object sender, RoutedEventArgs e)
        {
            dos = true;
        }

        private void Dos_Unchecked(object sender, RoutedEventArgs e)
        {
            dos = false;
        }

        private void Starvation_Unchecked(object sender, RoutedEventArgs e)
        {
            starvation = false;
        }

        private void Starvation_Checked(object sender, RoutedEventArgs e)
        {
            starvation = true;
        }

        private void Release_Checked(object sender, RoutedEventArgs e)
        {
            release = true;
        }

        private void Release_Unchecked(object sender, RoutedEventArgs e)
        {
            release = false;
        }

        private void Server_Unchecked(object sender, RoutedEventArgs e)
        {
            fakeServer = false;
        }

        private void Server_Checked(object sender, RoutedEventArgs e)
        {
            fakeServer = true;
        }

        private void Open_file(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Comma-separated values (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == true) {
                string[] parametrs = File.ReadAllText(openFileDialog.FileName).Split(',');
                serverIP.Text = (parametrs[0] != null) ? parametrs[0] : "null";
                serverMAC.Text = (parametrs[1] != null) ? parametrs[1] : "null";
                serverMask.Text = (parametrs[2] != null) ? parametrs[2] : "null";
                serverDNS.Text = (parametrs[3] != null) ? parametrs[3] : "null";
                MessageBox.Show("Інформанія проімпортована");
            }
        }

        private void Save_file(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Comma-separated values (*.csv)|*.csv";
            if (saveFileDialog.ShowDialog() == true) {
                File.WriteAllText(saveFileDialog.FileName, serverIP.Text
                    + ',' + serverMAC.Text + ',' + serverMask.Text
                    + ',' + serverDNS.Text + ',' + startIP.Text);
                MessageBox.Show("Інформація збережена");
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            work.setUnlim(true); 
            cout.IsEnabled = false;
        }

        private void unlimited_Unchecked(object sender, RoutedEventArgs e)
        {
            work.setUnlim(false); 
            cout.IsEnabled = true;
        }

    }
}
