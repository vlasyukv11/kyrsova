﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Net.NetworkInformation;

namespace DipMag_v1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Brush brush; // универсальная кисточка
        public MainWindow()
        {
            InitializeComponent();
            this.MaxHeight = 600;
            this.MaxWidth = 900;
            this.MinHeight = 600;
            this.MinWidth = 900;
            LogWriter logs = new LogWriter();
            logs.add("Початок роботи");
            //background
            var brushBg = new ImageBrush();
            brushBg.ImageSource = new BitmapImage(new Uri("images/fon.png", UriKind.Relative));
            this.Background = brushBg;

            //select box (adapters)
            brush = new SolidColorBrush(Colors.White);
            netAdapters.BorderBrush = brush;

            //start button (label)
            startButton.IsEnabled = false;
            startButton.Foreground = brush;
        }

        //список активних адаптерів
        private readonly List<NetworkInterface> activeInterfaces = new List<NetworkInterface>();
        private int numAdaper = 0;

        /// <summary>
        /// Пошук активних ІР адрес на клієнтському пк
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">RoutedEventArgs</param>
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            NetworkInterface[] interfaceList = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface @interface in interfaceList) {
                if (@interface.OperationalStatus == OperationalStatus.Up) {
                    netAdapters.Items.Add(@interface.Description);
                    activeInterfaces.Add(@interface);
                }
            }
        }

        private void netAdapters_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            brush = new SolidColorBrush(Colors.Red);

            startButton.IsEnabled = true;
            startButton.Foreground = brush;

            numAdaper = netAdapters.SelectedIndex;
        }

        private void startButton_MouseEnter(object sender, MouseEventArgs e)
        {
            if (startButton.IsEnabled) {
                brush = new SolidColorBrush(Colors.RosyBrown);
                startButton.Foreground = brush;
            }
        }

        private void startButton_MouseLeave(object sender, MouseEventArgs e)
        {
            if (startButton.IsEnabled) {
                brush = new SolidColorBrush(Colors.Red);
                startButton.Foreground = brush;
            }
        }

        private void startButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            WorkPlace wp = new WorkPlace(activeInterfaces[numAdaper]);
            wp.Show();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LogWriter logs = new LogWriter();
            logs.add("Закінчення роботи");
        }
    
    }
}
